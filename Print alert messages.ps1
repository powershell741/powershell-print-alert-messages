#Print alert messages by adding a new variable and then using an if statement to check the user input.

$answer = Read-Host -Prompt "Print the alert messages (y/n)?";
While (($answer -ne "y") -And ($answer -ne "n")) {
$answer = Read-Host -Prompt "Invalid input, please enter y or n";
}
If ($answer -Eq 'y') {
    write-Host ""
# code to print alert messages
}